# nelmio/alice

[**nelmio/alice**](https://packagist.org/packages/nelmio/alice) [428](https://phppackages.org/s/alice) Expressive fixtures generator

* [*Fixtures, the right gestures*](https://blog.theodo.fr/2019/04/fixtures-right-gestures/) 2019  Victor Lebrun
* [*How to manage fixtures in a PHP project*](https://blog.theodo.fr/2013/08/managing-fixtures/) 2013 Benjamin Grandfond
---
* (fr) [*Symfony — Créer des fixtures avec AliceBundle*
  ](https://gary-houbre.medium.com/symfony-cr%C3%A9er-des-fixtures-avec-alicebundle-a64e1d30c8ef)
  2021-08 Gary Houbre
